FROM alpine:3.12

RUN apk upgrade && \
    apk add nginx python3 py3-pip && \
    pip3 install python-dateutil humanize jinja2 python-gitlab rdflib

RUN install -d /run/nginx
RUN install -d -o nginx -g nginx /srv/www/gnome-metrics/htdocs
COPY Procfile /
COPY bin/ /app/bin/
COPY templates/ /app/templates/
COPY config/nginx/ /etc/nginx/

EXPOSE 5000
