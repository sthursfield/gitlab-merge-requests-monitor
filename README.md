# GNOME GitLab merge requests monitor

This project is an experiment. The goal is to help ensure GNOME contributors get
timely responses on merge requests.

To run the script, you first need to create [private access
token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
on GNOME GitLab, and save it in the file `secrets.env`.

Then, run a pipeline as follows:

    ./get_merge_requests --env-file secrets.env --count 2 | ./output_markdown ./templates/discourse.md

It takes some time to complete, probably due to API rate limiting -- use the
`--debug` flag to see what's happening.

The output will be a Markdown document like this:

    ## Recent merge requests in need of attention

    This list shows 10 merge requests which were opened a week or more ago but have not yet received feedback.

    ### evince - Stop trusting phsyical dimensions from monitors (Alternative Fix for #1403)

    Opened: Sep 10
    Author: [@example](https://gitlab.gnome.org/example)
    URL: https://gitlab.gnome.org/GNOME/evince/-/merge_requests/example

    ### rhythmbox - Remember column resizes

    Opened: Aug 21
    Author: [@example](https://gitlab.gnome.org/example)
    URL: https://gitlab.gnome.org/GNOME/rhythmbox/-/merge_requests/example

## Local testing

You can deploy a webapp that serves a list of unloved merge requests,
updated daily.

You can build and run the application locally using Docker or Podman, as shown
below. You'll need to modify the file `secrets.env` to contain a GitLab
personal access token. Open `secrets.env` to see how.

    podman build --tag gnome-metrics .
    podman run -i -t --env-file secrets.env --rm -p 5000:5000 gnome-metrics

The app will now be available at http://localhost:5000/

## Deployment to Dokku

The app is designed to be deployed from Git to a
[Dokku](http://dokku.viewdocs.io/dokku/) instance, like this:

    ssh dokku@example.com apps:create gnome-metrics
    ssh dokku@example.com storage:mount gnome-metrics /var/lib/dokku/data/storage:/srv/www/gnome-metrics
    ssh dokku@example.com config:set gnome-metrics DOKKU_PROXY_PORT_MAP='http:80:5000'
    ssh dokku@example.com config:set gnome-metrics GITLAB_PRIVATE_TOKEN=<your private token>
    
    git remote add deploy ssh://dokku@example.com/gnome-metrics
    git push deploy HEAD:master

Once deployed, you will need to set up a timer to update the page at the desired interval.
Use a systemd timer or cron to trigger the `update` command that we define in `Procfile`:

    /usr/bin/dokku run gnome-metrics update
