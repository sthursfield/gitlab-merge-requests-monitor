/* From: https://github.com/DataTables/Plugins/blob/master/dataRender/datetime.js
 * Modified to use moment.fromNow().
 */
// UMD
(function( factory ) {
	"use strict";

	if ( typeof define === 'function' && define.amd ) {
		// AMD
		define( ['jquery'], function ( $ ) {
			return factory( $, window, document );
		} );
	}
	else if ( typeof exports === 'object' ) {
		// CommonJS
		module.exports = function (root, $) {
			if ( ! root ) {
				root = window;
			}

			if ( ! $ ) {
				$ = typeof window !== 'undefined' ?
					require('jquery') :
					require('jquery')( root );
			}

			return factory( $, root, root.document );
		};
	}
	else {
		// Browser
		factory( jQuery, window, document );
	}
}
(function( $, window, document ) {


$.fn.dataTable.render.momentFromNow = function ( from, locale ) {
	// Argument shifting
	if ( arguments.length === 1 ) {
		locale = 'en';
	}

	return function ( d, type, row ) {
		if (! d) {
			return type === 'sort' || type === 'type' ? 0 : d;
		}

		var m = window.moment( d, from, locale, true );

		if ( type === 'sort' || type === 'type' ) {
			return m.format('x');
		} else {
			return m.fromNow();
		}
	};
};


}));
