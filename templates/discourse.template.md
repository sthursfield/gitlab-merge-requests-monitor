# Recent merge requests in need of attention.

This list shows some first-contribution merge requests which were opened a week
or more ago but have not yet received feedback.

{% for mr in merge_requests %}
### {{ mr.project_name }} - {{ mr.title }}

{{ mr.opened }}
Author: [{{ mr.author_username }}]({{mr.author_web_url}})
URL: <{{ mr.web_url }}>

{% endfor %}
